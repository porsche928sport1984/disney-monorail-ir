#include <IRremote.h>

IRrecv receiver(2); // receiver is connected to pin2
IRsend irsend;
decode_results results;

void (*reset)(void) = 0;

void setup() {
  Serial.begin(9600);
  receiver.enableIRIn();
}

void loop() {
  if(receiver.decode(&results)) {
    Serial.println(" ");
    Serial.println("HEX_VALUE: ");
    Serial.println(results.value, HEX);
    Serial.println("LENGTH_VALUE: ");
    Serial.println(results.rawlen);
    Serial.println("RAW_VALUE: ");
    for(int i = 1; i < results.rawlen; i++) {
      unsigned int number = results.rawbuf[i] * USECPERTICK;
      Serial.print(number);
      Serial.print(",");
    }
    Serial.println("");
    receiver.resume();
  }
}
